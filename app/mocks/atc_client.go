package mocks

import (
	//"fmt"
	"log"
	"bitbucket.org/cornjacket/airtrafficcontrol/client"
)

// Should I rename this clientMock
type AirTrafficControlServiceMock struct {
}

var AtcLastTallocReq	client.TAllocReq

func (s *AirTrafficControlServiceMock) Open(Hostname, Port, Path string) error {
	log.Println("AtcCientMock.Open() invoked.")
	return nil
}

func (s *AirTrafficControlServiceMock) RequestTslot(tallocreq *client.TAllocReq) (error) {
	log.Println("AtcClientMock.RequestTslot() invoked.")
	AtcLastTallocReq = *tallocreq 
	return nil
}
