package mocks

import (
	"fmt"
	"log"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot_app/view_client"
)

// Should I rename this clientMock
type ViewHndlrServiceMock struct {
}

var ViewLastRxPacket	message.UpPacket
var ViewLastData		view_client.Data 
var ViewLastNodeState	view_client.NodeState 

func (s *ViewHndlrServiceMock) Open(Hostname, Port string) error {
	log.Println("ViewClientMock.Open() invoked.")
	return nil
}

func (s *ViewHndlrServiceMock) SendPacket(packet message.UpPacket) (error) {
	log.Println("ViewClientMock.SendPacket() invoked.")
	ViewLastRxPacket = packet
	return nil 
}

func (s *ViewHndlrServiceMock) SendStatus(data view_client.Data) (error) {
	log.Println("ViewClientMock.SendStatus() invoked.")
	//fmt.Printf("ViewClientMock.SendStatus(): Data: %v\n", data)
	ViewLastData = data
	return nil
}

func (s *ViewHndlrServiceMock) SendNodestate(nodestate *view_client.NodeState) error {
	log.Println("ViewClientMock.SendNodestate() invoked.")
	fmt.Printf("ViewClientMock.SendSNodestate(): ActualA: %d, ActualB: %d\n", nodestate.ActualStateA, nodestate.ActualStateB)
	ViewLastNodeState = *nodestate
	return nil
}
