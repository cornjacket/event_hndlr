module bitbucket.org/cornjacket/event_hndlr/app

go 1.13

require (
	bitbucket.org/cornjacket/airtrafficcontrol/client v0.1.2
	bitbucket.org/cornjacket/event_hndlr/client v0.1.0
	bitbucket.org/cornjacket/iot v0.1.6
	bitbucket.org/cornjacket/iot_app/view_client v0.1.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f
)
