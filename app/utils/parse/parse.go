package parse 

import (
	"encoding/hex"
)

func Opcode(data_slice *[]byte) (byte, bool) {
	if len(*data_slice) < 1 {
	  return 0x00, false
	} else {
	  return (*data_slice)[0], true
	}
}

func StatusByte(data_slice *[]byte) (byte, bool) {
	if len(*data_slice) < 2 {
	  return 0x00, false
	} else {
	  return (*data_slice)[1], true
	}
}

// i need a function that takss hex string as input and returns array/slice of bytes  - second return type should be err
func HexStringToSlice(data string) ([]byte, bool) {
	// need to check for odd number of characters so to return an error
	empty := []byte("") // is there a bettery way to do this, i.e. returning an empty slice/array
	if len(data) < 2 {
	  //fmt.Println("PARSE\tERROR\tMalformed packet")
	  return empty, false
	} else {
	  opstr := []byte(data[0:len(data)])
	  oparr := make([]byte, hex.DecodedLen(len(opstr)))
	  _, err := hex.Decode(oparr, opstr)
	  if err != nil {
	    //fmt.Printf("PARSE\tERROR\tishex.Decode failed: %v\n", err)
	    return empty, false
	  }
	  return oparr, true
	}
	//fmt.Println("PARSE\tSTATUS\tpacket failed vMon check")
	return empty, false
}

