package actualstatea

import (
  "strconv"
)

type ActualStateA struct {
  value int
  updated bool
}

func New() *ActualStateA {
  return &ActualStateA{}
}

func (x *ActualStateA) IsUpdated() bool {
  return x.updated
}

func (x *ActualStateA) Get() int {
  return x.value
}

func (x *ActualStateA) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ActualStateA) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ActualStateA) String() string {
  return "ActualStateA"
}

func (x *ActualStateA) Type() string {
  return "int"
}

func (x *ActualStateA) Modifiers() string {
  return "not null"
}

func (x *ActualStateA) QueryValue() string {
  return strconv.Itoa(x.value)
}

