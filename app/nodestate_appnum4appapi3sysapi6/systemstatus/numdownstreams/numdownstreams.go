package numdownstreams

import (
  "strconv"
)

type NumDownstreams struct {
  value int
  updated bool
}

func New() *NumDownstreams {
  return &NumDownstreams{}
}

func (x *NumDownstreams) IsUpdated() bool {
  return x.updated
}

func (x *NumDownstreams) Get() int {
  return x.value
}

func (x *NumDownstreams) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *NumDownstreams) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *NumDownstreams) String() string {
  return "NumDownstreams"
}

func (x *NumDownstreams) Type() string {
  return "int"
}

func (x *NumDownstreams) Modifiers() string {
  return "not null"
}

func (x *NumDownstreams) QueryValue() string {
  return strconv.Itoa(x.value)
}

