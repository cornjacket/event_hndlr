package sysfwversion

import (
  "strconv"
)

type SysFwVersion struct {
  value int
  updated bool
}

func New() *SysFwVersion {
  return &SysFwVersion{}
}

func (x *SysFwVersion) IsUpdated() bool {
  return x.updated
}

func (x *SysFwVersion) Get() int {
  return x.value
}

func (x *SysFwVersion) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *SysFwVersion) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *SysFwVersion) String() string {
  return "SysFwVersion"
}

func (x *SysFwVersion) Type() string {
  return "int"
}

func (x *SysFwVersion) Modifiers() string {
  return "not null"
}

func (x *SysFwVersion) QueryValue() string {
  return strconv.Itoa(x.value)
}

