package lastrssi

import (
  "strconv"
)

type LastRssi struct {
  value int
  updated bool
}

func New() *LastRssi {
  return &LastRssi{}
}

func (x *LastRssi) IsUpdated() bool {
  return x.updated
}

func (x *LastRssi) Get() int {
  return x.value
}

func (x *LastRssi) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastRssi) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *LastRssi) String() string {
  return "LastRssi"
}

func (x *LastRssi) Type() string {
  return "int"
}

func (x *LastRssi) Modifiers() string {
  return "not null"
}

func (x *LastRssi) QueryValue() string {
  return strconv.Itoa(x.value)
}

