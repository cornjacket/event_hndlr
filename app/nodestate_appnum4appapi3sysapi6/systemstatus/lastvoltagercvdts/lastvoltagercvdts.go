package lastvoltagercvdts

import (
  "strconv"
)

type LastVoltageRcvdTs struct {
  value int64
  updated bool
}

func New() *LastVoltageRcvdTs {
  return &LastVoltageRcvdTs{}
}

func (x *LastVoltageRcvdTs) IsUpdated() bool {
  return x.updated
}

func (x *LastVoltageRcvdTs) Get() int64 {
  return x.value
}

func (x *LastVoltageRcvdTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastVoltageRcvdTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *LastVoltageRcvdTs) String() string {
  return "LastVoltageRcvdTs"
}

func (x *LastVoltageRcvdTs) Type() string {
  return "bigint"
}

func (x *LastVoltageRcvdTs) Modifiers() string {
  return "not null"
}

func (x *LastVoltageRcvdTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

