package systemdownstreamenable

import (
  "strconv"
)

type SystemDownstreamEnable struct {
  value int
  updated bool
}

func New() *SystemDownstreamEnable {
  return &SystemDownstreamEnable{}
}

func (x *SystemDownstreamEnable) IsUpdated() bool {
  return x.updated
}

func (x *SystemDownstreamEnable) Get() int {
  return x.value
}

func (x *SystemDownstreamEnable) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *SystemDownstreamEnable) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *SystemDownstreamEnable) String() string {
  return "SystemDownstreamEnable"
}

func (x *SystemDownstreamEnable) Type() string {
  return "tinyint"
}

func (x *SystemDownstreamEnable) Modifiers() string {
  return "not null"
}

func (x *SystemDownstreamEnable) QueryValue() string {
  return strconv.Itoa(x.value)
}

