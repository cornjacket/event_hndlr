package controllers

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/cornjacket/iot/message"

	"github.com/julienschmidt/httprouter"
	_ "github.com/go-sql-driver/mysql"
	atc_client "bitbucket.org/cornjacket/airtrafficcontrol/client"
	"bitbucket.org/cornjacket/iot_app/view_client"
)

type ViewHandlrPostInterface interface {
	Open(Hostname, Port string) (error)
	SendStatus(data view_client.Data) (error)
	SendNodestate(nodestate *view_client.NodeState) (error)
	SendPacket(packet message.UpPacket) (error) 
}

type AirTrafficControlPostInterface interface {
	Open(Hostname, Port, Path string) (error)
	//SendPacket(packet message.UpPacket) (error)  // Not needed for EventHndlr
	RequestTslot(tallocreq *atc_client.TAllocReq) (error)
}

// TODO: change guild so that it doesn't use package level scope. Instead create a type which lives in the AppContext
type AppContext struct {
	Router *httprouter.Router
	ViewHndlrService ViewHandlrPostInterface
	//AirTrafficControlService atc_client.AirTrafficControlService
	AirTrafficControlService AirTrafficControlPostInterface
}

func NewAppContext() *AppContext {
	context := AppContext{}
	context.ViewHndlrService = &view_client.ViewHndlrService{} // implements ViewHandlrPostInterface
	context.AirTrafficControlService = &atc_client.AirTrafficControlService{} // implements AirTrafficControlPostInterface
	return &context
}

func (app *AppContext) Run(addr string) {
	// Instantiate a new router
        app.Router = httprouter.New()

	app.initializeRoutes()

	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}
