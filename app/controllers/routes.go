package controllers

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"github.com/gorilla/context"
)

func (app *AppContext) initializeRoutes() {

	// Home Route
	app.Router.GET("/", wrapHandlerFunc(app.Home))

	// Packet Route
	app.Router.POST("/packet", app.NewParserWorkerPoolFunc(100))

	// TallocResp Router 
	app.Router.POST("/tallocResp.4.3.6", app.NewTallocRespPoolEntryFunc(100))

	// TODO:
	// Add /ping
	// Add /health
	// Add version, lastboot, ... via status. See code in temp

}

// Wrapper function to make http handlers (i.e. goriall/mux) work with httprouter (i.e. julienschmidt)
// gorilla/context may be useful in the future if other http handlers (besides home) need to be converted to httprouter.
// source: https://www.nicolasmerouze.com/guide-routers-golang 
func wrapHandlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}
