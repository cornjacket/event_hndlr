package ack

import (
  "strconv"
)

type Ack struct {
  value int
  updated bool
}

func New() *Ack {
  return &Ack{}
}

func (x *Ack) IsUpdated() bool {
  return x.updated
}

func (x *Ack) Get() int {
  return x.value
}

func (x *Ack) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Ack) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Ack) String() string {
  return "Ack"
}

func (x *Ack) Type() string {
  return "tinyint"
}

func (x *Ack) Modifiers() string {
  return "not null"
}

func (x *Ack) QueryValue() string {
  return strconv.Itoa(x.value)
}

