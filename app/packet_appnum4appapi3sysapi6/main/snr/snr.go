package snr

import (
  "strconv"
)

type Snr struct {
  value int
  updated bool
}

func New() *Snr {
  return &Snr{}
}

func (x *Snr) IsUpdated() bool {
  return x.updated
}

func (x *Snr) Get() int {
  return x.value
}

func (x *Snr) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Snr) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Snr) String() string {
  return "Snr"
}

func (x *Snr) Type() string {
  return "int"
}

func (x *Snr) Modifiers() string {
  return "not null"
}

func (x *Snr) QueryValue() string {
  return strconv.Itoa(x.value)
}

