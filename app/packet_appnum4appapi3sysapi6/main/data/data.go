package data

import (
)

type Data struct {
  value string
  updated bool
}

func New() *Data {
  return &Data{}
}

func (x *Data) IsUpdated() bool {
  return x.updated
}

func (x *Data) Get() string {
  return x.value
}

func (x *Data) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Data) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *Data) String() string {
  return "Data"
}

func (x *Data) Type() string {
  return "varchar(22)"
}

func (x *Data) Modifiers() string {
  return "not null"
}

func (x *Data) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

