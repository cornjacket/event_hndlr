package freq

import (
  "strconv"
)

type Freq struct {
  value int64
  updated bool
}

func New() *Freq {
  return &Freq{}
}

func (x *Freq) IsUpdated() bool {
  return x.updated
}

func (x *Freq) Get() int64 {
  return x.value
}

func (x *Freq) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Freq) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *Freq) String() string {
  return "Freq"
}

func (x *Freq) Type() string {
  return "bigint"
}

func (x *Freq) Modifiers() string {
  return "not null"
}

func (x *Freq) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

