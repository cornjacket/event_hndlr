package dr

import (
)

type Dr struct {
  value string
  updated bool
}

func New() *Dr {
  return &Dr{}
}

func (x *Dr) IsUpdated() bool {
  return x.updated
}

func (x *Dr) Get() string {
  return x.value
}

func (x *Dr) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Dr) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *Dr) String() string {
  return "Dr"
}

func (x *Dr) Type() string {
  return "varchar(22)"
}

func (x *Dr) Modifiers() string {
  return "not null"
}

func (x *Dr) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

