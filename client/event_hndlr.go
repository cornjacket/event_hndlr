package client 

import (

	"fmt"
	"encoding/json"
	"bytes"
	"errors"
	"net/http"
	"io/ioutil"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type EventHndlrService struct {
	Hostname	string
	Port      	string
	PacketPath      string
	TallocRespPath	string
	Enabled		bool
}

// TODO: Validate Hostname and Port within reason
func (s *EventHndlrService) Open(Hostname, Port, PacketPath, TallocRespPath string) error {
	s.Hostname = Hostname
	s.Port = Port
	s.PacketPath = PacketPath
	s.TallocRespPath = TallocRespPath
	s.Enabled = true
	return s.Ping()	
}

func (s *EventHndlrService) URL(path string) string {
	if s.Enabled {
		return "http://" + s.Hostname + ":" + s.Port + path 
	}
	return ""
}

// TODO: This needs to be done...
// Used to determine if the dependent service is currently up.
// Ping should make multiple attempts to see if the external service is up before giving up and returning an error.
func (s *EventHndlrService) Ping() (error) {
	// check if enabled
	// if so, then make a GET call to /ping of the URL()
	// if there is an error, then repeatedly call for X times with linear backoff until success
/* Keep and add to services...
        // ping loop
        var pingErr error
        for i := 0; i < 15; i++ {
                fmt.Printf("Pinging %s\n", DbHost)
                pingErr = server.DB.DB().Ping()
                if pingErr != nil {
                        fmt.Println(pingErr)
                } else {
                        fmt.Println("Ping replied.")
                        break;
                }
                time.Sleep(time.Duration(3) * time.Second)
        }
        if pingErr != nil {
                log.Fatal("This is the error:", err)
        }
*/

	return nil
}

func (s *EventHndlrService) SendPacket(packet message.UpPacket) (error) {

	var err error
	if s.Enabled {
		//fmt.Printf("PacketEventHndlrService: Sending packet to %s\n", s.URL(s.PacketPath))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)
		err = Post(s.URL(s.PacketPath), b)
	} else {
		fmt.Printf("PacketEventHndlrService is disabled. Dropping packet.\n")
	}
	return err 

}

func (s *EventHndlrService) SendTallocResp(tallocresp tallocrespproc.TallocResp) (error) {

	var err error
	if s.Enabled {
		//fmt.Printf("TallocRespEventHndlrService: Sending tallocresp to %s\n", s.URL(s.TallocRespPath))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(tallocresp)
		err = Post(s.URL(s.TallocRespPath), b)
	} else {
		fmt.Printf("TallocRespEventHndlrService is disabled. Dropping packet.\n")
	}
	return err 

}

// This function can be extracted and included in the uService clientlib for a Post function
// post will return error on non 200 or 201 status code
func Post(url string, b *bytes.Buffer) (error) {
	var res *http.Response
	var err error
	res, err = http.Post(url, "application/json; charset=utf-8", b) // Should I defer close of the res.Body
	//fmt.Printf("statusCreated: %d\n", http.StatusCreated)
	//fmt.Printf("statusOK: %d\n", http.StatusOK)
	if err == nil {
		//fmt.Printf("http_post nil: statusCode: %d\n", res.StatusCode)
		if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusCreated {
			body, _ := ioutil.ReadAll(res.Body)
			err = errors.New(string(body))
		}
	}
	return err
}
