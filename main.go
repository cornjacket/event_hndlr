package main

import (
	"bitbucket.org/cornjacket/event_hndlr/app"
)

func main() {

	eventApp := app.NewAppService()	
	eventApp.Init()
	eventApp.Run()

}

